const express = require('express');
const bodyParser = require('body-parser');
const mongoose = require('mongoose')
const dotenv = require('dotenv');

dotenv.config();

console.log("MONGO---> ", process.env.MONGO);

const routes = require('./router');
const app = express();

app.use(bodyParser.urlencoded({ extended: false}));
// parse application/json
app.use(bodyParser.json());
routes(app);

const PORT = process.env.PORT || 4000;

mongoose
.connect(process.env.MONGO, {
  useNewUrlParser: true, 
  useUnifiedTopology: true
})
.then( () => {
  console.log("Connected to mongodb");
  app.listen(PORT, () => {
    console.log("Running on port ", PORT);
  });
})
.catch(error => {
  console.log("Mongo error", error);
})