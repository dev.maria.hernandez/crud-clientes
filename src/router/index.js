const clientesRoutes = require('./clientes');

module.exports = (app) => {
    app.use('/api/clientes', clientesRoutes)
};