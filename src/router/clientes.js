const express = require("express");
const clientesControllers = require("../controllers/clientes"); 

const router = express.Router();

router.post('/NutriNET/Cliente', clientesControllers.createCliente);
router.get('/NutriNET/Cliente', clientesControllers.getCliente);
router.get('/NutriNET/Cliente/:clientId', clientesControllers.getClienteByclienteId);
router.put('/NutriNET/Cliente/:clientId', clientesControllers.updateCliente);

module.exports = router;