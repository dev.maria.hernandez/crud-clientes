const mongoose = require('mongoose');

//const Schemas = mongoose.Schema;
const {Schema} = mongoose;
const clienteSchema =  new Schema({
    nombre_Usuario: { type: String, required: true},
    contrasenia: { type: String, required: true},
    nombre: {type: String, required: true, unique: true},
    apellidos: {type: String, required: true},
    correo_Electronico:  { type: String, required: true, unique: true},
    edad: {type: Number},
    estatura: {type: Number},
    peso: {type: Number},
    IMC: {type: Number},
    GEB: {type: Number},
    ETA: {type: Number},
    // fecha_Creacion: {type: Date},
    // fecha_Actualizacion: {type: Date},
});

const model = mongoose.model('Clientes', clienteSchema);

module.exports = model;