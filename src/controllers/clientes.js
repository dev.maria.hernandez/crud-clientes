
const Clientes = require('../mongo/models/clientes')


const createCliente = async (req, res) => {
    try{
        const body = req.body;
        const {nombre_Usuario, contrasenia, nombre, apellidos, correo_Electronico, edad, estatura, peso, IMC, GEB, ETA} = body;
        var fecha_Creacion = new Date();
        var fecha_Actualizacion = new Date();
        
        await Clientes.create({
            nombre_Usuario,
            contrasenia, 
            nombre,
            apellidos,
            correo_Electronico,
            edad,
            estatura,
            peso, 
            IMC,
            GEB,
            ETA,
            fecha_Creacion,
            fecha_Actualizacion
        });

        res.send({
            Cve_Error: 0,
            Cve_Mensaje: 'Client created'
        });
    }
    catch(error){
        if( error.code && error.code === 11000){
            res.status(400).send({
                Cve_Error: -1,
                Cve_Mensaje: 'DUPLICATED_VALUES' + error.keyValue
            });
            return;
        }
        res.status(400).send({
            Cve_Error: -2,
            Cve_Mensaje: "error create client"
            
        });
    }
};


const getCliente = async (req, res) => {
    try{
        const clientes = await Clientes.find();
        res.send({
            Cve_Error: 0,
            data: clientes
        });
    }catch(e){
        res.status(500).send({
            Cve_Error: -3,
            Cve_Mensaje: "Internal Error"
            
        });
    }
};

const getClienteByclienteId = async (req, res) => {
    try{
        const clienteId = req.params.clientId;
        const clientes = await Clientes.find({
            _id: clienteId
        });
        console.log(clientes);
        const user = clientes[clienteId];
        console.log(user);
        if(clientes.length == 0){
            res.status(404).send({
                Cve_Error: -4,
                Cve_Mensaje: "Client not found"
            });
        }
        res.send({
            Cve_Error: '0',
            data: clientes
        });
    }catch(e){
        res.status(404).send({
            Cve_Error: -4,
            Cve_Mensaje: "Client not found"
        });
    }
}

const updateCliente = async (req, res) => {
    try{
        const clienteId = req.params.clientId;
        const {nombre_Usuario, contrasenia, nombre, apellidos, correo_Electronico, edad, estatura, peso, IMC, GEB, ETA} = req.body;
        var fecha_Actualizacion = new Date();
        await Clientes.findByIdAndUpdate(clienteId,{
            nombre_Usuario,
            contrasenia,
            nombre,
            apellidos,
            correo_Electronico,
            edad,
            estatura,
            peso, 
            IMC,
            GEB,
            ETA,
            fecha_Actualizacion
        });
        res.send({
            Cve_Error: 0,
            Cve_Mensaje: 'Client update'
        });
    }catch(error){
        if( error.code && error.code === 11000){
            res.status(400).send({
                Cve_Error: -1,
                Cve_Mensaje: 'DUPLICATED_VALUES' + error.keyValue
            });
            return;
        }
        res.status(500).send({
            Cve_Error: -3,
            Cve_Mensaje: 'Internal error'
        });
    }
};

module.exports = {
    createCliente, 
    getCliente, 
    getClienteByclienteId,
    updateCliente,
};

